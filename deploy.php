<?php
declare(strict_types=1);

namespace Deployer;

require 'recipe/symfony4.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'https://gitlab.com/thomasage/smallpwa');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', ['.env.local']);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);
set('allow_anonymous_stats', false);

set('keep_releases', 1);

// Hosts

inventory('hosts.yaml');

// Tasks

task(
    'deploy:assets',
    static function (): void {
        runLocally('yarn run encore prod');
        upload('public/build', '{{release_path}}/public');
    }
);

before('deploy:writable', 'deploy:assets');

// [Optional] if deploy fails automatically unlock.

after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

//before('deploy:symlink', 'database:migrate');

// [Optional] Specific to OVH

set('bin/php', '/usr/local/php7.3/bin/php');
