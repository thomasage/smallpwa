import Vue from 'vue'
import VueRouter from 'vue-router'

import Homepage from './pages/Homepage'
import InterventionAdd from './pages/InterventionAdd'
import InterventionEdit from './pages/InterventionEdit'

const publicPath = '/app'

Vue.use(VueRouter)

const routes = [
  {
    component: Homepage,
    name: 'homepage',
    path: publicPath
  },
  {
    component: InterventionAdd,
    name: 'intervention_add',
    path: `${publicPath}/intervention/add`
  },
  {
    component: InterventionEdit,
    name: 'intervention_edit',
    path: `${publicPath}/intervention/edit/:intervention`
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
