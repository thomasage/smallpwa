console.log('Hello from service worker.')

import { precacheAndRoute } from 'workbox-precaching'
import { registerRoute } from 'workbox-routing/registerRoute.mjs'
import { ExpirationPlugin } from 'workbox-expiration/ExpirationPlugin.mjs'
import { CacheFirst, NetworkFirst } from 'workbox-strategies'

precacheAndRoute(self.__WB_MANIFEST)

registerRoute(
  /\.(?:png|gif|jpg|jpeg|svg|ico)$/,
  new CacheFirst({
    cacheName: 'images',
    plugins: [
      new ExpirationPlugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
      }),
    ],
  }),
)
registerRoute(/\/app/, new NetworkFirst)
