import Dexie from 'dexie'
import { v4 as uuidv4 } from 'uuid'

const DB_NAME = 'smallpwa'
const DB_VERSION = 1

class AppStorage {

  constructor () {
    this.db = new Dexie(DB_NAME)
    this.db.version(DB_VERSION).stores({
      interventions: '++uuid,site'
    })
  }

  addIntervention (intervention) {
    intervention.uuid = uuidv4()
    return this.db.interventions.add(intervention)
  }

  getIntervention (uuid) {
    return this.db.interventions.get(uuid)
  }

  getInterventions () {
    return this.db.interventions.toArray()
  }

  updateIntervention (uuid, payload) {
    return this.db.interventions.update(uuid, payload)
  }

}

export default new AppStorage()
