import Vue from 'vue'

import vuetify from './plugins/vuetify'

import router from './app-router'
import store from './app-store'

import App from './App'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/service-worker.js')
    .then(registration => {
      console.log('SW registered. Scope is', registration.scope)
    })
    .catch(error => {
      console.warn('Unable to register SW', error)
    })
}
