<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AppController extends AbstractController
{
    /**
     * @Route("/app/{route}",
     *     methods={"GET"},
     *     requirements={"route"=".*"},
     *     defaults={"route"=""})
     */
    public function __invoke(): Response
    {
        return $this->render('base.html.twig');
    }
}
